// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyGameMode.h"
#include "TimerManager.h"
#include "PuzzlePlatformsGameInstance.h"

void ALobbyGameMode::BeginPlay()
{
	Super::BeginPlay();
}

void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	++NumPlayers;
	if (NumPlayers > 2)
	{
		GetWorldTimerManager().SetTimer(TransferTimer, this, &ALobbyGameMode::TriggerTransfer, 10.0f, false, -1.0f);
	}
}

void ALobbyGameMode::TriggerTransfer()
{
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr))
	{
		return;
	}

	UPuzzlePlatformsGameInstance* GameInstance = Cast<UPuzzlePlatformsGameInstance>(GetGameInstance());
	GameInstance->StartSession();

	UE_LOG(LogTemp, Warning, TEXT("Game starting after trigger transfer!"));
	bUseSeamlessTravel = true;
	World->ServerTravel("/Game/PuzzlePlatforms/Maps/Game?listen");
}

void ALobbyGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);
	--NumPlayers;
}


