// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/PanelWidget.h"
#include "ServerLineWidget.generated.h"

/**
 * 
 */
UCLASS()
class PUZZLEPLATFORMS_API UServerLineWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* ServerName;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* PlayerCount;

	UPROPERTY(meta = (BindWidget))
	class UButton* RowButton;

	UPROPERTY(BlueprintReadOnly)
	bool Selected = false;

	UFUNCTION()
	void SelectRow();
	
	void Setup(class UMainMenu* Parent, uint32 Index);
	
protected:

	virtual bool Initialize() override;

private:

	uint32 WidgetIndex;

	UPROPERTY()
	class UMainMenu* WidgetParent;
};
