// Fill out your copyright notice in the Description page of Project Settings.

#include "InGameMenu.h"
#include "Components/Button.h"


bool UInGameMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(CancelToButton != nullptr)) return false;
	CancelToButton->OnClicked.AddDynamic(this, &UInGameMenu::CancelInGameMenu);

	if (!ensure(QuitButton != nullptr)) return false;
	QuitButton->OnClicked.AddDynamic(this, &UInGameMenu::QuitToMain);

	return true;
}

void UInGameMenu::CancelInGameMenu()
{
	this->RemoveFromViewport();

	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr))
	{
		return;
	}

	FInputModeGameOnly GameMode;
	PlayerController->SetInputMode(GameMode);
	PlayerController->bShowMouseCursor = false;
}

void UInGameMenu::QuitToMain()
{
	if (MenuInterface != nullptr)
	{
		MenuInterface->QuitToMainMenu();
	}
}

