// Fill out your copyright notice in the Description page of Project Settings.


#include "ServerLineWidget.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "MainMenu.h"

bool UServerLineWidget::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(RowButton != nullptr)) return false;
	RowButton->OnClicked.AddDynamic(this, &UServerLineWidget::SelectRow);

	return true;
}

void UServerLineWidget::SelectRow()
{
	WidgetParent->SelectIndex(WidgetIndex);
	UE_LOG(LogTemp, Warning, TEXT("Selected row %d"), WidgetIndex);
}

void UServerLineWidget::Setup(class UMainMenu* Parent, uint32 Index)
{
	WidgetIndex = Index;
	WidgetParent = Parent;
}

